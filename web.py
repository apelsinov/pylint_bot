from flask import Flask, request, abort
from flask import render_template
from db import db_connection

app = Flask(__name__)


@app.route("/supersecreturl")
def list_checks():
    if request.args.get("show") == "1":
        cursor = db_connection().cursor()
        cursor.execute("SELECT * FROM requests;")

        result = []

        for row in cursor.fetchall():
            result.append(row)

        cursor.close()

        return render_template("checks.html", items=result)
    else:
        abort(404)

@app.route("/config")
def test_config():
    return "MAX_PROCESSES=6\nRERUNS=4"
