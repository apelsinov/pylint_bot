import os
import psycopg2

from psycopg2.extensions import ISOLATION_LEVEL_AUTOCOMMIT


def create_database():
    sql = "CREATE DATABASE requests;"""

    connection = psycopg2.connect(
        user=os.getenv("DB_USER"),
        password=os.getenv("DB_PASSWORD"),
        host="db",
        port="5432",
    )

    connection.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)
    cursor = connection.cursor()

    try:
        cursor.execute(sql)
        connection.close()
    except psycopg2.errors.DuplicateDatabase as e:
        print(e)

def db_connection():
    create_database()

    connection = psycopg2.connect(
        user=os.getenv("DB_USER"),
        password=os.getenv("DB_PASSWORD"),
        host="db",
        port="5432",
        database="requests"
    )

    connection.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)
    cursor = connection.cursor()

    sql_table = """
        CREATE TABLE requests (
            id SERIAL PRIMARY KEY,
            user_id TEXT,
            username TEXT,
            repo_url TEXT,
            date timestamp DEFAULT CURRENT_TIMESTAMP
        );
        """

    try:
        cursor.execute(sql_table.strip())
    except psycopg2.errors.DuplicateTable as e:
        print(e)

    return connection


if __name__ == "__main__":
    cursor = db_connection().cursor()
    cursor.execute("SELECT * FROM requests;")

    for row in cursor.fetchall():
        print(row)

    cursor.close()
