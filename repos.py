import shutil
import os
import urllib.request
import urllib.error

import pycodestyle
from git import Repo
from git.exc import GitCommandError, GitCommandNotFound, InvalidGitRepositoryError
from pylint import epylint


def get_repo_and_branch(repo_url: str, branch: str = "master", *a):
    repo_hash = str(hash(repo_url))[1:]
    hash_path = f"tmp/{repo_hash}"

    try:
        urllib.request.urlopen(repo_url)
    except (urllib.error.HTTPError, ValueError, urllib.error.URLError):
        return None, None, None, "Такой репы нет!"

    try:
        Repo.clone_from(repo_url, hash_path).git.checkout(branch)
    except (GitCommandError, GitCommandNotFound, InvalidGitRepositoryError) as error:
        shutil.rmtree(hash_path, ignore_errors=True)
        return None, None, None, error

    return hash_path, repo_url, branch, None


def run_pylint_for(hash_path):
    pylint_stdout, _ = epylint.py_run(
        f'{hash_path} --msg-template=\"({{msg_id}}:{{category}}) {{path}}#L{{line}} {{msg}}\" '
        f'-d C0114,C0116,R0903,E0401,C0411,C0103,C0115,W0707,R0201,W0621', return_std=True)

    # Filtering useless elements in raw results
    return [msg for msg in pylint_stdout.getvalue().split("\n")
            if msg.strip() and "*************" not in msg and "-------------" not in msg]


def run_pycodestyle_for(hash_path):
    return pycodestyle.StyleGuide(quiet=True).check_files(paths=[hash_path])


def dir_to_list(hash_path):
    folders, items_to_return = [], []

    with os.scandir(hash_path) as scan:
        for entry in sorted(scan, key=lambda entry: entry.name):
            is_dir = entry.is_dir()

            if entry.name in [".git"]: continue
            item_type = 'directory' if is_dir else 'file'

            if item_type == 'directory':
                folders.append(entry.name)

            item = {'type': item_type, 'name': entry.name}

            if is_dir:
                item['contents'] = dir_to_list(os.path.join(hash_path, entry.name))

            items_to_return.append(item)

    shutil.rmtree(hash_path, ignore_errors=True)
    return items_to_return, folders
