import os

from telegram.ext import Updater
from telegram import Update
from telegram.ext import CallbackContext
from telegram.ext import CommandHandler
from repos import run_pylint_for, dir_to_list, run_pycodestyle_for, get_repo_and_branch
from db import db_connection


def get_token():
    env_token = os.getenv("TOKEN", None)

    if env_token:
        return env_token.strip()

    with open("token") as f:
        return f.read().strip()


updater = Updater(token=get_token(), use_context=True)
dispatcher = updater.dispatcher


def start(update: Update, context: CallbackContext):
    context.bot.send_message(chat_id=update.effective_chat.id,
                             text="Codestyle bot by MIKHAIL C. v1.3\n"
                                  "To check your repo 'master' branch use `/lint https://github.com/user/repo.git` command.\n"
                                  "To check specific branch use `/lint https://github.com/user/repo.git feature_branch` command.\n")

def lint(update: Update, context: CallbackContext):
    if len(context.args) < 1:
        return None, None, None

    hash_path, repo_url, branch, err = get_repo_and_branch(*context.args)

    if err is not None:
        context.bot.send_message(
            chat_id=update.effective_chat.id,
            text=f"Something wrong with repo or branch.\nError: {err}"
        )
        return None
    
    if repo_url.endswith(".git"):
        repo_url = repo_url[:-4]

    con = db_connection()
    con.cursor().execute("INSERT INTO requests (user_id, username, repo_url) VALUES(%s, %s, %s)",
                (update.effective_chat.id, update.effective_chat.username, context.args[0]))
    con.close()

    context.bot.send_message(
        chat_id=update.effective_chat.id,
        text=f"Got your repo. Pylinting..."
    )

    context.bot.send_message(chat_id=update.effective_chat.id, text="********** PYLINT **********")

    pylint_results = run_pylint_for(hash_path)
    pylint_results = [msg.strip().replace(hash_path.split("/")[1], f"{repo_url}/blob/{branch}")
                      for msg in pylint_results if msg.strip().startswith("(") or msg.strip().startswith("==")]

    if not pylint_results:
        context.bot.send_message(chat_id=update.effective_chat.id, text="All ok.")
    else:
        for line in pylint_results:
            context.bot.send_message(chat_id=update.effective_chat.id, text=line, disable_web_page_preview=True)

    context.bot.send_message(chat_id=update.effective_chat.id, text="********** PYCODESTYLE **********")

    pycodestyle_results = run_pycodestyle_for(hash_path)
    template = "({category}) {path} {msg}"
    pycodestyle_results = [
        template.format(
            category=key,
            path=pycodestyle_results.filename.replace(hash_path, f"{repo_url}/blob/{branch}"),
            msg=pycodestyle_results.messages[key]
        ) for key in pycodestyle_results.messages
    ]

    if not pycodestyle_results:
        context.bot.send_message(chat_id=update.effective_chat.id, text="All ok.")
    else:
        for line in pycodestyle_results:
            if line.strip():
                context.bot.send_message(chat_id=update.effective_chat.id, text=line.strip(), disable_web_page_preview=True)

    context.bot.send_message(chat_id=update.effective_chat.id, text="********** STRUCTURE **********")

    initial_score = 10

    directory_structure, folders = dir_to_list(hash_path)
    for folder in [".idea", "venv", "env", "allure-report", "allure-results", ".pytest_cache", "__pycache__", ".vscode"]:
        if folder in folders:
            initial_score -= 1
            context.bot.send_message(
                chat_id=update.effective_chat.id,
                text=f"===> (-1) {folder} folder must NOT be in repository."
            )

    first_level_files = []
    for item in directory_structure:
        if item.get("type") == "file":
            first_level_files.append(item.get("name"))

    for file_name in ["README.md", ".gitignore", "requirements.txt"]:
        if file_name not in first_level_files:
            initial_score -= 1
            context.bot.send_message(
                chat_id=update.effective_chat.id,
                text=f"===> (-1) {file_name} file MUST BE PRESENT on first level of the repository."
            )

    if "pytest.ini" not in first_level_files:
        context.bot.send_message(
            chat_id=update.effective_chat.id,
            text=f"===> Use 'pytest.ini' on first level of the repository for configuration."
        )
    else:
        initial_score += 0.5

    for file_name in first_level_files:
        if file_name.startswith("test_"):
            initial_score -= 1
            context.bot.send_message(
                chat_id=update.effective_chat.id,
                text="===> (-1) All test modules (files started with test_) MUST BE in tests/ folder."
            )
            break

    for file_name in first_level_files:
        if file_name.endswith(".log"):
            initial_score -= 1
            context.bot.send_message(
                chat_id=update.effective_chat.id,
                text="===> (-1) All .log files must be absent."
            )

    context.bot.send_message(chat_id=update.effective_chat.id, text=f"Structure score: {initial_score} of 10.")

start_handler = CommandHandler('start', start)
lint_handler = CommandHandler('lint', lint)

dispatcher.add_handler(start_handler)
dispatcher.add_handler(lint_handler)

updater.start_polling()
